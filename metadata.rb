name             'gitlab-elasticsearch'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact+cookbooks@gitlab.com'
license          'MIT'
description      'Cookbook gitlab-elasticsearch for GitLab cookbooks'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'
chef_version     '>= 12.1' if respond_to?(:chef_version)
issues_url       'https://gitlab.com/gitlab-cookbooks/gitlab_users/issues'
source_url       'https://gitlab.com/gitlab-cookbooks/gitlab_users'

supports 'ubuntu', '= 16.04'

depends 'gitlab_secrets'
depends 'chef-vault'
depends 'elasticsearch'
